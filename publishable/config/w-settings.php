<?php

return [
    'text-fields' => [
        0 => [
            'field' => 'text-field',
            'type' => 'text',
            'val' => 'sdadads',
        ],
        1 => [
            'field' => 'textarea',
            'type' => 'textarea',
            'val' => '2222',
        ],
        2 => [
            'field' => 'medium-editor',
            'type' => 'editor',
            'val' => '<div><br></div><p class=""><br></p><p class="">adsfad</p><p class="">as asdf</p><p class="">as dasdf</p><p class="">editor<br></p><p class="">editor<br></p><p class=""><br></p><p class=""><br></p><p class=""><br></p>',
        ],
        3 => [
            'field' => 'number',
            'type' => 'number',
            'val' => 12,
            'options' => [
                'min' => 10,
                'max' => 20,
            ],
        ],
    ],
    'select-fields' => [
        0 => [
            'field' => 'switch',
            'type' => 'switch',
            'val' => false,
            'values' => [
                0 => 'Disabled',
                1 => 'Activated',
            ],
        ],
        1 => [
            'field' => 'radio-buttons',
            'type' => 'radio',
            'val' => 1,
            'values' => [
                0 => 'Culture',
                1 => 'Religion',
                2 => 'Art',
                3 => 'History',
                4 => 'Science',
                5 => 'Fiction',
            ],
        ],
        2 => [
            'field' => 'checkboxes',
            'type' => 'checkbox',
            'val' => [
                0 => 5,
                1 => 1,
                2 => 2,
                3 => 3,
            ],
            'values' => [
                0 => 'Culture',
                1 => 'Religion',
                2 => 'Art',
                3 => 'History',
                4 => 'Science',
                5 => 'Fiction',
            ],
        ],
        3 => [
            'field' => 'select',
            'type' => 'select',
            'val' => 1,
            'values' => [
                0 => [
                    'id' => 1,
                    'label' => 'Culture',
                ],
                1 => [
                    'id' => 2,
                    'label' => 'Religion',
                ],
                2 => [
                    'id' => 3,
                    'label' => 'Art',
                ],
                3 => [
                    'id' => 4,
                    'label' => 'History',
                ],
            ],
        ],
        4 => [
            'field' => 'multiple-select',
            'type' => 'multiple-select',
            'val' => [
                0 => 0,
                1 => 1,
            ],
            'values' => [
                0 => [
                    'id' => 0,
                    'label' => 'Culture',
                ],
                1 => [
                    'id' => 1,
                    'label' => 'Religion',
                ],
                2 => [
                    'id' => 2,
                    'label' => 'Art',
                ],
                3 => [
                    'id' => 3,
                    'label' => 'History',
                ],
            ],
        ],
        5 => [
            'field' => 'cascade',
            'type' => 'cascade',
            'val' => [
                0 => 1,
                1 => 3,
            ],
            'values' => [
                0 => [
                    'value' => 1,
                    'label' => 'Phones',
                    'children' => [
                        0 => [
                            'value' => 2,
                            'label' => 'Samsung',
                        ],
                        1 => [
                            'value' => 3,
                            'label' => 'Apple',
                        ],
                    ],
                ],
                1 => [
                    'value' => 4,
                    'label' => 'Laptops',
                    'children' => [
                        0 => [
                            'value' => 5,
                            'label' => 'Asus',
                        ],
                        1 => [
                            'value' => 6,
                            'label' => 'Razer',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'special-fields' => [
        0 => [
            'field' => 'password',
            'type' => 'password',
            'val' => '$2y$10$arL7bqYhooRM2c5u.nDmQuGdNzrhZuB0.oJfjqgA7JVFEBlIDEBLq',
            'rules' => 'min:8',
        ],
        1 => [
            'field' => 'slider',
            'type' => 'slider',
            'val' => 30,
            'options' => [
                'min' => 10,
                'max' => 50,
                'step' => 5,
            ],
        ],
        2 => [
            'field' => 'rate',
            'type' => 'rate',
            'val' => 4,
        ],
    ],
    'date-time-fields' => [
        0 => [
            'field' => 'date',
            'type' => 'date',
            'val' => '2017-03-24',
        ],
        1 => [
            'field' => 'date-time',
            'type' => 'date-time',
            'val' => '2018-03-13 17:57:16',
        ],
        2 => [
            'field' => 'time',
            'type' => 'time',
            'val' => '15:02:06',
        ],
        3 => [
            'field' => 'time-select',
            'type' => 'time-select',
            'val' => '16:30',
            'options' => [
                'start' => '09:00',
                'end' => '18:00',
                'step' => '00:30',
            ],
        ],
    ],
    'media-fields' => [
        0 => [
            'field' => 'image',
            'type' => 'image',
            'val' => 214,
        ],
        1 => [
            'field' => 'gallery',
            'type' => 'multiple-images',
            'val' => [
                0 => 64,
                1 => 65,
                2 => 162,
                3 => 163,
                4 => 164,
                5 => 176,
                6 => 180,
                7 => 181,
            ],
        ],
    ],
];