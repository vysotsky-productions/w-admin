<?php
return [
    'model' => 'VY\\WAdmin\\Models\\Media',
    'name' => 'Media',
    'slug' => 'media',

    'fields' => [
        'main' => [
            [
                'field' => 'path',
                'type' => 'image-by-path',
                'disabled' => true
            ],
            [
                'field' => 'id',
                'type' => 'number',
                'disabled' => true
            ],
            [
                'field' => 'name',
                'type' => 'text'
            ],
            [
                'field' => 'path',
                'type' => 'text',
                'disabled' => true
            ],
        ],
        'additional' => [
            [
                'field' => 'extension',
                'type' => 'text',
                'disabled' => true
            ],
            [
                'field' => 'width',
                'type' => 'text',
                'disabled' => true
            ],
            [
                'field' => 'height',
                'type' => 'text',
                'disabled' => true
            ],
            [
                'field' => 'size',
                'type' => 'text',
                'disabled' => true
            ],
            [
                'field' => 'type',
                'type' => 'text',
                'disabled' => true
            ],
        ],
        'time' => [
            [
                'field' => 'created_at',
                'type' => 'date-time',
                'disabled' => true
            ],
            [
                'field' => 'updated_at',
                'type' => 'date-time',
                'disabled' => true
            ],
        ]
    ],

    'list' => [
        [
            'field' => 'id',
            'type' => 'text',
        ],
        [
            'field' => 'name',
            'type' => 'text'
        ],
        [
            'field' => 'path',
            'type' => 'text'
        ],
        [
            'field' => 'type',
            'type' => 'text'
        ],
    ]
];