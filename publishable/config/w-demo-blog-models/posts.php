<?php
return [
    'model' => 'App\Models\Posts',
    'name' => 'Posts',
    'slug' => 'posts',
    'search' => ['id', 'slug', 'title', 'annotation'],

    'fields' => [
        'main' => [
            [
                'field' => 'id',
                'type' => 'text',
                'disabled' => true
            ],
            [
                'field' => 'title',
                'type' => 'text',
                'rules' => 'required'
            ],
            [
                'field' => 'slug',
                'type' => 'text',
                'rules' => 'required|alpha_dash:en'
            ],
            [
                'field' => 'annotation',
                'type' => 'textarea',
                'rules' => 'required'
            ],
            [
                'field' => 'image',
                'type' => 'image',
                'media_type' => 'post'
            ]
        ],

        'relations' => [
            [
                'field' => 'author_id',
                'type' => 'select',
                'relationship' => [
                    'type' => 'belongsTo',
                    'model' => 'App\Models\Authors',
                    'local_key' => 'author_id',
                    'foreign_key' => 'id',
                    'field_name' => 'name',
                ]
            ],
            [
                'field' => 'category_id',
                'type' => 'select',
                'relationship' => [
                    'type' => 'belongsTo',
                    'model' => 'App\Models\Categories',
                    'local_key' => 'category_id',
                    'foreign_key' => 'id',
                    'field_name' => 'name',
                ]
            ],
            [
                'field' => 'tags',
                'type' => 'multiple-select',
                'relationship' => [
                    'type' => 'belongsToMany',
                    'model' => 'App\Models\Tags',
                    'table' => 'posts_tags',
                    'pivot_local_key' => 'post_id',
                    'pivot_foreign_key' => 'category_id',
                    'field_name' => 'name',
                ]
            ],
        ],
        'content' => [
            [
                'field' => 'content',
                'type' => 'editor'
            ],
        ],
        'time' => [
            [
                'field' => 'published',
                'type' => 'date-time'
            ], [
                'field' => 'created_at',
                'type' => 'date-time',
                'disabled' => true
            ], [
                'field' => 'updated_at',
                'type' => 'date-time',
                'disabled' => true
            ]
        ]
    ],

    'list' => [
        [
            'field' => 'id',
            'type' => 'text',
        ],
        [
            'field' => 'slug',
            'type' => 'text'
        ],
        [
            'field' => 'title',
            'type' => 'text'
        ],
        [
            'field' => 'published',
            'type' => 'text'
        ],
    ]
];