<?php
return [
    'model' => 'App\Models\Roles',
    'name' => 'Roles',
    'slug' => 'roles',

    'fields' => [
        'main' => [
            [
                'field' => 'id',
                'type' => 'number',
                'disabled' => true
            ],
            [
                'field' => 'name',
                'type' => 'text',
            ],
            [
                'field' => 'slug',
                'type' => 'text',
            ],
        ],
        'time' => [
            [
                'field' => 'created_at',
                'type' => 'date-time',
                'disabled' => true
            ],
            [
                'field' => 'updated_at',
                'type' => 'date-time',
                'disabled' => true
            ],
        ]
    ],

    'list' => [
        [
            'field' => 'id',
            'type' => 'text',
        ],
        [
            'field' => 'name',
            'type' => 'text'
        ],
        [
            'field' => 'slug',
            'type' => 'text'
        ],
    ]
];