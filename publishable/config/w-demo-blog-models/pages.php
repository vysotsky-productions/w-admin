<?php
return [
    'model' => 'App\Models\Pages',
    'name' => 'Pages',
    'slug' => 'pages',
    'search' => ['id', 'slug', 'title', 'annotation'],

    'fields' => [
        'main' => [
            [
                'field' => 'id',
                'type' => 'text',
                'disabled' => true
            ],
            [
                'field' => 'title',
                'type' => 'text',
                'rules' => 'required'
            ],
            [
                'field' => 'slug',
                'type' => 'text',
                'rules' => 'required|alpha_dash:en'
            ],
            [
                'field' => 'annotation',
                'type' => 'textarea',
                'rules' => 'required'
            ],
            [
                'field' => 'image',
                'type' => 'image'
            ],
            [
                'field' => 'gallery',
                'type' => 'multiple-images',
                'relationship' => [
                    'type' => 'gallery',
                    'table' => 'pages_media',
                    'pivot_local_key' => 'page_id',
                    'pivot_foreign_key' => 'media_id',
                ]
            ]
        ],
        'content' => [
            [
                'field' => 'content',
                'type' => 'editor'
            ],
        ],
        'time' => [
            [
                'field' => 'published',
                'type' => 'date-time'
            ], [
                'field' => 'created_at',
                'type' => 'date-time',
                'disabled' => true
            ], [
                'field' => 'updated_at',
                'type' => 'date-time',
                'disabled' => true
            ]
        ]
    ],

    'list' => [
        [
            'field' => 'id',
            'type' => 'text',
        ],
        [
            'field' => 'slug',
            'type' => 'text'
        ],
        [
            'field' => 'title',
            'type' => 'text'
        ],
        [
            'field' => 'published',
            'type' => 'text'
        ],
    ]
];