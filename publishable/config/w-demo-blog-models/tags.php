<?php
return [
    'model' => 'App\Models\Tags',
    'name' => 'Tags',
    'slug' => 'tags',
    'search' => ['id', 'name', 'slug'],

    'fields' => [
        'main' => [
            [
                'field' => 'id',
                'type' => 'text',
                'disabled' => true
            ],
            [
                'field' => 'name',
                'type' => 'text',
                'rules' => 'required'
            ],
            [
                'field' => 'slug',
                'type' => 'text',
                'rules' => 'required|alpha_dash:en'
            ],
        ],
        'time' => [
            [
                'field' => 'created_at',
                'type' => 'date-time',
                'disabled' => true
            ], [
                'field' => 'updated_at',
                'type' => 'date-time',
                'disabled' => true
            ]
        ]

    ],

    'list' => [
        [
            'field' => 'id',
            'type' => 'text',
        ],
        [
            'field' => 'slug',
            'type' => 'text'
        ],
        [
            'field' => 'name',
            'type' => 'text'
        ]
    ]
];