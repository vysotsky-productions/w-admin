<?php
return [
    'model' => 'App\Models\Authors',
    'name' => 'Authors',
    'slug' => 'Authors',
    'search' => ['id', 'name', 'slug', 'about'],

    'fields' => [
        'main' => [
            [
                'field' => 'id',
                'type' => 'text',
                'disabled' => true
            ],
            [
                'field' => 'name',
                'type' => 'text',
                'rules' => 'required'
            ],
            [
                'field' => 'slug',
                'type' => 'text',
                'rules' => 'required|alpha_dash:en'
            ],
            [
                'field' => 'about',
                'type' => 'textarea',
                'rules' => 'required'
            ],
            [
                'field' => 'image',
                'type' => 'image'
            ]
        ],

        'time' => [
            [
                'field' => 'birthday',
                'type' => 'date-time'
            ],
            [
                'field' => 'created_at',
                'type' => 'date-time',
                'disabled' => true
            ],
            [
                'field' => 'updated_at',
                'type' => 'date-time',
                'disabled' => true
            ]
        ]
    ],

    'list' => [
        [
            'field' => 'id',
            'type' => 'text',
        ],
        [
            'field' => 'slug',
            'type' => 'text'
        ],
        [
            'field' => 'name',
            'type' => 'text'
        ]
    ]
];