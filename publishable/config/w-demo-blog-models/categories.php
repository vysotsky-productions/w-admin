<?php
return [
    'model' => 'App\Models\Categories',
    'name' => 'Categories',
    'slug' => 'categories',
    'search' => ['id', 'name'],

    'fields' => [
        'main' => [
            [
                'field' => 'id',
                'type' => 'text',
                'disabled' => true
            ],
            [
                'field' => 'name',
                'type' => 'text',
                'rules' => 'required'
            ],
            [
                'field' => 'slug',
                'type' => 'text',
                'rules' => 'required|alpha_dash:en'
            ],
            [
                'field' => 'parent_id',
                'type' => 'select',
                'relationship' => [
                    'type' => 'belongsTo',
                    'model' => 'App\Models\Categories',
                    'local_key' => 'parent_id',
                    'foreign_key' => 'id',
                    'field_name' => 'name',
                ]
            ],
        ],
        'time' => [
            [
                'field' => 'created_at',
                'type' => 'date-time',
                'disabled' => true
            ], [
                'field' => 'updated_at',
                'type' => 'date-time',
                'disabled' => true
            ]
        ]

    ],

    'list' => [
        [
            'field' => 'id',
            'type' => 'text',
        ],
        [
            'field' => 'slug',
            'type' => 'text'
        ],
        [
            'field' => 'name',
            'type' => 'text'
        ],
        [
            'field' => 'parent_id',
            'type' => 'text'
        ],
    ]
];