<?php
return [
    'model' => 'VY\WAdmin\Models\User',
    'name' => 'Users',
    'slug' => 'users',

    'fields' => [
        'main' => [
            [
                'field' => 'id',
                'type' => 'number',
                'disabled' => true
            ],
            [
                'field' => 'name',
                'type' => 'text',
            ],
            [
                'field' => 'email',
                'type' => 'text',
            ],
            [
                'field' => 'password',
                'type' => 'password',
            ],
            [
                'field' => 'role_id',
                'type' => 'select',
                'relationship' => [
                    'type' => 'belongsTo',
                    'model' => 'App\Models\Roles',
                    'local_key' => 'user_id',
                    'foreign_key' => 'id',
                    'field_name' => 'name',
                ]
            ],
        ],
        'time' => [
            [
                'field' => 'created_at',
                'type' => 'date-time',
                'disabled' => true
            ],
            [
                'field' => 'updated_at',
                'type' => 'date-time',
                'disabled' => true
            ],
        ]
    ],

    'list' => [
        [
            'field' => 'id',
            'type' => 'text',
        ],
        [
            'field' => 'name',
            'type' => 'text'
        ],
        [
            'field' => 'email',
            'type' => 'text'
        ],
        [
            'field' => 'role_id',
            'type' => 'text'
        ],
    ]
];