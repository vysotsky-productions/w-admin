<?php

return [
    'post' => [
        'small' => [
            'width' => 200,
            'height' => 200,
            'position' => 'center'
        ],
        'big' => [
            'width' => 800,
            'height' => 600,
            'position' => 'top'
        ]
    ]
];