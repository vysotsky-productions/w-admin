<?php

return [
    'path' => 'w-admin',
    'appName' => 'Demo Blog',

    'models' => [
        'blog' => [
            'posts' => [
                'name' => 'Posts',
                'icon' => 'fa-th-list'
            ],
            'categories' => [
                'name' => 'Categories',
                'icon' => 'fa-folder-open'
            ],
            'tags' => [
                'name' => 'Tags',
                'icon' => 'fa-tags'
            ],
            'authors' => [
                'name' => 'Authors',
                'icon' => 'fa-user'
            ],
        ],
        'users' => [
            'users' => [
                'name' => 'Users',
                'icon' => 'fa-users '
            ],
            'roles' => [
                'name' => 'Roles',
                'icon' => 'fa-lock'
            ],
        ],
        'content' => [
            'pages' => [
                'name' => 'Pages',
                'icon' => 'fa-file'
            ],
            'media' => [
                'name' => 'Media',
                'icon' => 'fa-images'
            ],
        ],
    ],
];
