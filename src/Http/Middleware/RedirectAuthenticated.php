<?php

namespace VY\WAdmin\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class RedirectAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect(config('w-admin.path') . '/login');
        }

        return $next($request);
    }
}
