<?php

namespace VY\WAdmin\Http\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function __construct()
    {
        View::share('admin_panel_route', config('w-admin')['path']);
    }

    public function template()
    {
        return view('w-admin::views.layouts.index');
    }
}