<?php

namespace VY\WAdmin\Http\Controllers;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use VY\WAdmin\Models\Media as MediaModel;
use Carbon\Carbon;

class Media extends Controller
{
    public function save()
    {
        $media = new MediaModel();
        $file = current(Request::allFiles());

        if (isset($file)) {
            $now = Carbon::now();

            $storeDirectory = 'media/' . $now->year . '/' . $now->format('m');

            $media->name = $file->getClientOriginalName();
            $media->path = '/storage/' . Storage::disk('public')->put($storeDirectory, $file);
            $this->thumbnails();

            /** Server URL with HTTP or HTTPS */
            $serverUrl = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'];

            /** Getting width, height, file size and extension */
            $info = getimagesize($serverUrl . $media->path);
            $extension = str_replace('.', '', image_type_to_extension($info[2]));
            $size = get_headers($serverUrl . $media->path, 1)["Content-Length"];

            $media->extension = $extension;
            $media->size = $size;
            $media->width = $info[0];
            $media->height = $info[1];
            $media->type = Request::get('type', 'default');

            if ($media->save()) {
                return response()->json(['status' => 'success', 'notification' => 'Media file successfully saved!', 'media' => $media], 200);
            } else {
                return response()->json(['status' => 'error', 'notification' => 'Error when uploading!'], 200);
            }
        }
    }

    public function saveFromEditor()
    {
        $media = new MediaModel();
        $file = Request::file('files')[0];

        if (isset($file)) {
            $now = Carbon::now();

            $storeDirectory = 'media/' . $now->year . '/' . $now->format('m');
            $this->thumbnails();

//            $media->name = $file->getClientOriginalName();
            $media->name = '';
            $media->extension = '';
            $media->path = '/storage/' . Storage::disk('public')->put($storeDirectory, $file);

            $media->size = 0;
            $media->width = 0;
            $media->height = 0;
            $media->type = Request::get('type', 'editor');

            if ($media->save()) {
                return response()->json(['files' => [['url' => $media->path]], 'status' => 'success', 'notification' => 'Media file successfully saved!', 'media' => $media], 200);
            } else {
                return response()->json(['status' => 'error', 'notification' => 'Error when uploading!'], 200);
            }
        }
    }

    public function thumbnails()
    {

    }
}