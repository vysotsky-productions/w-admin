<?php

namespace VY\WAdmin\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Schema;

use VY\WAdmin\Traits\FieldTrait;
use VY\WAdmin\Traits\ModelsTraits;

class Model extends BaseController
{
    use ModelsTraits;
    use FieldTrait;

    public function get($slug, $id = 0)
    {
        $model = config('w-models.' . $slug);
        $item = false;

        if (!empty($model)) {
            if ($id) {
                $item = $model['model']::find($id);
            }

            $list = $this->getList($model);
            $countAllItems = $this->countAllItem($model);
            $model['fields'] = $this->modelsFieldsProcessing($model['fields'], $item);

            return response()->json([
                'status' => 'success',
                'model' => $model,
                'list' => $list,
                'countAllItems' => $countAllItems,
                'item_exists' => $item ? true : false
            ], 200);
        }

        return response()->json(['status' => 'error', 'notification' => 'Model not found'], 404);
    }

    public function chartData(\Illuminate\Http\Request $request)
    {
        $model = config('w-models.' . $request->table);
        for ($i = 0; $i < 32; $i++) {
            if (!empty($model)) {
                $date = $request->year . '-' . $request->month;

                $dates = $model['model']::whereBetween('created_at', [$date . '-01', $date . '-31'])
                    ->groupBy('created_at')->get(['created_at']);
                $returnData = [];
                foreach ($dates as $date) {
                    $date = explode(' ', $date->created_at);
                    if (!isset($returnData[$date[0]])) {
                        $returnData[$date[0]] = 0;
                    } else {
                        $returnData[$date[0]]++;
                    }
                }
                return response()->json($returnData, 200);
            }
        }
        return response()->json(['success' => 'true', 'notification' => 'Model not found'], 404);

    }

    public function remove($slug, $id)
    {
        $model = config('w-models.' . $slug);

        if (!empty($model)) {
            $this->detach($model, $id);
            $model['model']::destroy($id);

            $list = $this->getList($model);
            $countAllItems = $this->countAllItem($model);

            return response()->json([
                'status' => 'success',
                'notification' => 'Item successfully removed!',
                'list' => $list,
                'countAllItems' => $countAllItems
            ], 200);
        }

        return response()->json(['status' => 'error', 'notification' => 'Model not found'], 404);
    }

    public function detach($model, $id)
    {
        $item = $model['model']::find($id);

        foreach ($model['fields'] as $tabName => &$tab) {
            foreach ($tab as $fieldName => &$field) {
                if (!empty($field['relationship']) && ($field['relationship']['type'] === 'belongsToMany' || $field['type'] === 'multiple-images' || $field['type'] === 'multiple-files')) {
                    $item->{$field['field']}()->detach();
                }
            }
        }
    }


    public function save($slug, $id)
    {
        $model = config('w-models.' . $slug);

        if ($id) {
            $item = $model['model']::find($id);
        } else {
            $item = new $model['model']();
        }

        $relations = [];
        $newData = Request::get('fields');

        foreach ($model['fields'] as $tabName => &$tab) {
            foreach ($tab as $fieldName => &$field) {
                if ((isset($field['disabled']) && $field['disabled']) || $field['type'] === 'link') {
                    continue;
                } else {
                    if (isset($newData[$tabName][$fieldName]['val'])) {
                        $value = $newData[$tabName][$fieldName]['val'];
                    } else {
                        if (!empty($field['default'])) {
                            $value = $field['default'];
                        } else {
                            $value = null;
                        }
                    }
                    if ($field['type'] === 'password') {
                        if (strlen($newData[$tabName][$fieldName]['val']) > 0) {
                            $item->{$field['field']} = Hash::make($value);
                        }
                    } else if ($field['type'] === 'editor') {
                        $item->{$field['field']} = $this->clearHtml($value);
                    } else if ($field['type'] === 'image' || $field['type'] === 'file') {
                        $item->{$field['field']} = $value['id'];
                    } else if ($field['type'] === 'multiple-images' || $field['type'] === 'multiple-files') {
                        $relations[] = $newData[$tabName][$fieldName];
                    } else if (!empty($field['relationship']) && $field['relationship']['type'] === 'hasMany') {
                        $relations[] = $newData[$tabName][$fieldName];
                    } else if (!empty($field['relationship']) && $field['relationship']['type'] === 'belongsToMany') {
                        $relations[] = $newData[$tabName][$fieldName];
                    } else if (!empty($field['relationship']) && $field['relationship']['type'] === 'hasOne') {
                        $relations[] = $newData[$tabName][$fieldName];
                    } else {
                        $item->{$field['field']} = $value;
                    }
                }
            }
        }

        if ($item->save()) {
            foreach ($relations as $relation) {
                if ($relation['type'] === 'multiple-images' || $field['type'] === 'multiple-files') {
                    $item->{$relation['field']}()->detach();
                    $item->{$relation['field']}()->attach(array_pluck($relation['val'], 'id'));
                } else if ($relation['relationship']['type'] === 'belongsToMany') {
                    $item->{$relation['field']}()->detach();
                    $item->{$relation['field']}()->attach($relation['val']);
                } else if ($relation['relationship']['type'] === 'hasMany') {
                    $item = $item->load($relation['field']);

                    // Remove old relation
                    foreach ($item->{$relation['field']} as $relItem) {
                        $relItem->{$relation['relationship']['foreign_key']} = 0;
                        $relItem->save();
                    }

                    // Add new relation
                    if (!empty($relation['val'])) {
                        foreach ($relation['val'] as $selectedItems) {
                            $relItem = $relation['relationship']['model']::where('id', $selectedItems)->first();
                            $relItem->{$relation['relationship']['foreign_key']} = $item->id;
                            $relItem->save();
                        }
                    }
                } else if ($relation['relationship']['type'] === 'hasOne') {
                    $item = $item->load($relation['field']);

                    // Remove old relation
                    if ($item->{$relation['field']}) {
                        $item->{$relation['field']}->{$relation['relationship']['foreign_key']} = 0;
                        $item->{$relation['field']}->save();
                    }

                    // Add new relation
                    if ($relation['val']) {
                        $relItem = $relation['relationship']['model']::where('id', $relation['val'])->first();

                        $relItem->{$relation['relationship']['foreign_key']} = $item->id;
                        $relItem->save();
                    }
                }
            }

            $list = $this->getList($model);
            $countAllItems = $this->countAllItem($model);
            $model['fields'] = $this->modelsFieldsProcessing($model['fields'], $item);

            return response()->json(['status' => 'success',
                'notification' => 'Model data saved!',
                'list' => $list,
                'model' => $model,
                'id' => $item->id,

                'countAllItems' => $countAllItems
            ], 200);
        }
    }
}