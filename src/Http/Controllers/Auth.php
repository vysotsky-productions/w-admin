<?php

namespace VY\WAdmin\Http\Controllers;

use Illuminate\Support\Facades\Auth as FacadeAuth;
use Illuminate\Support\Facades\Request;

class Auth extends Controller
{
    public function login()
    {
        return view('w-admin::views.layouts.index');
    }

    public function authenticate()
    {
        if (FacadeAuth::attempt(['email' => Request::get('email'), 'password' => Request::get('password')])) {
            return response()->json(['status' => 'authenticated'], 200);
        }

        return response()->json(['status' => 'error'], 401);
    }

    public function logout()
    {
        FacadeAuth::logout();
        return redirect('/' . config('w-admin.path') . '/login');
    }

    public function getUser()
    {
        if (FacadeAuth::id()) {
            return response()->json([
                'status' => 'success',
                'user' => FacadeAuth::user(),
                'settings' => config('w-admin')
            ], 200);
        } else {
            return response()->json(['status' => 'error'], 401);
        }
    }
}