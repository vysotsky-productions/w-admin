<?php

namespace VY\WAdmin\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use VY\WAdmin\Traits\FieldTrait;
use VY\WAdmin\Traits\SettingsTrait;

class Settings extends Controller
{
    use FieldTrait;
    use SettingsTrait;

    public function get()
    {
        if (file_exists(config_path('w-settings.php'))) {
            $settings = require(config_path('w-settings.php'));

            $settings = $this->fieldsProcessing($settings);

            return response()->json($settings);
        }

        return response()->json(['message' => 'Settings file not found'], 404);
    }

    public function save()
    {
        $newSettings = Request::all();

        if (file_exists(config_path('w-settings.php'))) {
            $settings = require(config_path('w-settings.php'));

            foreach ($settings as $tabName => &$tab) {
                foreach ($tab as $fieldName => &$field) {
                    $value = $newSettings[$tabName][$fieldName]['val'];

                    if ($field['type'] === 'password') {
                        if (strlen($newSettings[$tabName][$fieldName]['val']) > 0) {
                            $field['val'] = Hash::make($value);
                        }
                    } else if ($field['type'] === 'editor') {
                        $field['val'] = $this->clearHtml($value);
                    } else if ($field['type'] === 'image' || $field['type'] === 'file') {
                        $field['val'] = $value['id'];
                    } else if ($field['type'] === 'multiple-images' || $field['type'] === 'multiple-files') {
                        $field['val'] = array_pluck($value, 'id');
                    } else {
                        $field['val'] = $value;
                    }
                }
            }

            file_put_contents(config_path('w-settings.php'), "<?php \n\nreturn " . $this->varexport($settings) . ";");
            return response()->json(['status' => 'success', 'notification' => 'Settings are successfully saved!'], 200);
        }

        return response()->json(['status' => 'error', 'notification' => 'Settings file not found!'], 404);
    }


}