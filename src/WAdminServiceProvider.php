<?php

namespace VY\WAdmin;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use VY\WAdmin\Http\Middleware\Authenticated;
use VY\WAdmin\Http\Middleware\RedirectAuthenticated;


class WAdminServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     **/

    public function boot(Router $router)
    {
        $this->publishes([
            __DIR__ . '/../publishable/config' => config_path(),
        ], 'config');
        $this->publishes([
            __DIR__ . '/../publishable/public/' => 'public/vendor/vy/w-admin/public'
        ], 'javascript');


        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');


        $this->loadViewsFrom(__DIR__ . '/../resources', 'w-admin');
        $router->aliasMiddleware('admin.logedin', RedirectAuthenticated::class);
        $router->aliasMiddleware('admin.authenticated', Authenticated::class);

        if ($this->app->runningInConsole()) {
            $this->registerConsoleCommands();
        }

        require __DIR__ . '/../routes/w-router.php';
    }

    private function registerConsoleCommands()
    {
        $this->commands(Commands\UserCommand::class);
    }
}
