<?php

namespace VY\WAdmin\Models;

class Media extends \Eloquent
{
    protected $fillable = [
        'path', 'name', 'password', 'extension', 'width', 'height', 'type'
    ];
}