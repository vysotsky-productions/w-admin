<?php

namespace VY\WAdmin\Traits;

use VY\WAdmin\Models\Media;

trait FieldTrait
{
    use RelationsTrait;

    public function modelsFieldsProcessing($fields, $model = false)
    {
        foreach ($fields as $tabName => &$tab) {
            foreach ($tab as $fieldName => &$field) {
                $field['tab'] = $tabName;
                $field['val'] = '';

                if ($field['type'] === 'password') {
                    continue;
                } else if ($field['type'] === 'image' || $field['type'] === 'file') {
                    if ($model && !empty($model->{$field['field']})) {
                        $field['val'] = Media::find($model{$field['field']});
                    }
                } else if ($field['type'] === 'date-time') {
                    if ($model && is_object($model->{$field['field']})) {
                        $field['val'] = $model->{$field['field']}->toDateTimeString();
                    } else if ($model) {
                        $field['val'] = $model->{$field['field']};
                    }
                } else if (!empty($field['relationship']) || $field['type'] === 'multiple-images' || $field['type'] === 'multiple-files') {
                    $field = $this->assignRelation($field, $model);
                } else {
                    if ($model) {
                        $field['val'] = $model->{$field['field']};
                    }
                }
            }
        }

        return $fields;
    }

    public function assignRelation($field, $model)
    {
        $relType = $field['relationship']['type'];
        $field['values'] = [];

        if ($relType === 'belongsTo') {
            $field = $this->belongsTo($field, $model);
        } else if ($relType === 'hasOne' && $field['type'] === 'select') {
            $field = $this->hasOne($field, $model);
        } else if ($relType === 'hasMany') {
            $field = $this->hasMany($field, $model);
        } else if ($relType === 'belongsToMany') {
            $field = $this->belongsToMany($field, $model);
        } else if ($relType === 'gallery') {
            $field = $this->gallery($field, $model);
        } else if ($relType === 'hasOne' && $field['type'] === 'link') {
            $field = $this->link($field, $model);
        }

        return $field;
    }

    function clearHtml($buffer)
    {
        $search = [
            '/\>[^\S ]+/s',     // strip whitespaces after tags, except space
            '/[^\S ]+\</s',     // strip whitespaces before tags, except space
            '/(\s)+/s',         // shorten multiple whitespace sequences
            '/<!--(.|\s)*?-->/' // Remove HTML comments
        ];

        $replace = [
            '>',
            '<',
            '\\1',
            ''
        ];

        $buffer = preg_replace($search, $replace, $buffer);

        return $buffer;
    }
}
