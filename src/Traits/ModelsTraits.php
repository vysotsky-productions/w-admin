<?php

namespace VY\WAdmin\Traits;

use Illuminate\Support\Facades\Request;

/**
 * @property  \Illuminate\Database\Eloquent\Collection roles
 */
trait ModelsTraits
{
    public function getList($model)
    {
        $list = [];
        $itemsPerPage = Request::get('items_per_page', 30);
        $pageNumber = Request::get('page', 1);

        $query = new $model['model']();
        $query = $this->search($query, $model);
        $query = $this->order($query);

        $orederBy = isset($model['default_order']['field']) ? $model['default_order']['field'] : 'id';
        $orderDirection = isset($model['default_order']['direction']) ? $model['default_order']['direction'] : 'asc';

        $selectList = [];
        $relationsList = [];

        foreach ($model['list'] as $item) {
            if (!isset($item['relation'])) {
                $selectList[] = $item['field'];
            } else {
                $relationsList[] = $item['relation'];
            }
        }

        $select = $query->select($selectList)
            ->skip(($pageNumber - 1) * $itemsPerPage)
            ->with($relationsList)
            ->take($itemsPerPage)->orderBy($orederBy, $orderDirection)->get()->toArray();

        // todo: refactor list types
        foreach ($select as $rowId => $row) {
            foreach ($model['list'] as $field) {
                if (!isset($field['relation']) && isset($row[$field['field']])) {
                    $list[$rowId][$field['field']] = $row[$field['field']];
                } else if (isset($field['relation'])) {
                    $list[$rowId][$field['relation'] . '.' . $field['field']] = $row[$field['relation']][$field['field']];
                }
            }
        }

        return $list;
    }

    public function order($query)
    {
        $orderField = Request::get('orderField', null);
        $orderDirection = Request::get('orderDirection', null);

        if ($orderField && $orderDirection) {
            $query = $query->orderBy($orderField, $orderDirection);
        }

        return $query;
    }

    public function search($query, $model)
    {
        $searchPhrase = Request::get('searchPhrase', null);

        if (!empty($model['search']) && $searchPhrase) {
            foreach ($model['search'] as $fieldKey => $field) {
                $query = $query->orWhere($field, 'like', '%' . $searchPhrase . '%');
            }
        }

        return $query;
    }

    public function countAllItem($model)
    {
        $query = new $model['model']();
        $query = $this->search($query, $model);

        return $query->count();
    }
}
