<?php

namespace VY\WAdmin\Traits;

trait RelationsTrait
{
    public function belongsTo($field, $model)
    {
        $field['val'] = 0;

        if ($model) {
            $field['val'] = (int)$model->{$field['field']};
        }

        if (isset($field['disabled']) && $field['disabled']) {
            if ($relData = $field['relationship']['model']::where($field['relationship']['foreign_key'], $field['val'])->first()) {
                $field['values'][] = [
                    'id' => $relData->id,
                    'label' => $this->label($relData, $field['relationship']['field_name'])
                ];
            }
        } else {
            if ($relData = $field['relationship']['model']::get()) {
                foreach ($relData as $item) {
                    $field['values'][] = [
                        'id' => $item->id,
                        'label' => $this->label($item, $field['relationship']['field_name'])
                    ];
                }
            }
        }


        return $field;
    }

    public function hasOne($field, $model)
    {
        $field['val'] = 0;

        if ($model) {
            $model = $model->load($field['field']);

            if ($model->{$field['field']}) {
                $field['val'] = $model->{$field['field']}->id;
            }
        }

        if (isset($field['disabled']) && $field['disabled']) {
            if ($relData = $field['relationship']['model']::where($field['relationship']['local_key'], $field['val'])->first()) {
                $field['values'][] = [
                    'id' => $relData->id,
                    'label' => $this->label($relData, $field['relationship']['field_name'])
                ];
            }
        } else {
            if ($relData = $field['relationship']['model']::get()) {
                foreach ($relData as $item) {
                    $field['values'][] = [
                        'id' => $item->id,
                        'label' => $this->label($item, $field['relationship']['field_name'])
                    ];
                }
            }
        }


        return $field;
    }

    public function hasMany($field, $model)
    {
        $field['val'] = [];

        if ($model) {
            $model = $model->load($field['field']);
            foreach ($model->{$field['field']} as $item) {
                $field['val'][] = $item->id;
            }
        }

        if (isset($field['disabled']) && $field['disabled']) {
            foreach ($field['val'] as $val) {
                if ($relData = $field['relationship']['model']::where($field['relationship']['local_key'], $val)->first()) {
                    $field['values'][] = [
                        'id' => $relData->id,
                        'label' => $this->label($relData, $field['relationship']['field_name'])
                    ];
                }
            }
        } else {
            if ($relData = $field['relationship']['model']::get()) {
                foreach ($relData as $item) {
                    $field['values'][] = [
                        'id' => $item->id,
                        'label' => $this->label($item, $field['relationship']['field_name'])
                    ];
                }
            }
        }


        return $field;
    }

    public function belongsToMany($field, $model)
    {
        $field['val'] = [];

        if ($model) {
            $model = $model->load($field['field']);
            $tempRel = $model->{$field['field']};

            foreach ($tempRel as $item) {
                $field['val'][] = (int)$item->id;
            }
        }

        if (isset($field['disabled']) && $field['disabled']) {
            foreach ($field['val'] as $val) {
                if ($relData = $field['relationship']['model']::where('id', $val)->first()) {
                    $field['values'][] = [
                        'id' => $relData->id,
                        'label' => $this->label($relData, $field['relationship']['field_name'])
                    ];
                }
            }
        } else {
            if ($relData = $field['relationship']['model']::get()) {
                foreach ($relData as $item) {
                    $field['values'][] = [
                        'id' => $item->id,
                        'label' => $this->label($item, $field['relationship']['field_name'])
                    ];
                }
            }
        }


        return $field;
    }

    public function gallery($field, $model)
    {
        $field['val'] = [];

        if ($model) {
            $model = $model->load($field['field']);
            $field['val'] = $model->{$field['field']};
        }

        return $field;
    }

    public function link($field, $model)
    {
        $field['val'] = '';

        $relModel = $field['relationship']['model']::where($field['relationship']['foreign_key'], $model[$field['relationship']['local_key']])->first();

        if ($relModel) {
            $field['val'] = $relModel->{$field['relationship']['field_name']};
        }

        return $field;
    }

    public function label($item, $field)
    {
        $label = '';
        if (is_array($field)) {
            foreach ($field as $fieldLabelKey => $fieldLabel) {
                if ($item->{$fieldLabel}) {
                    if ($fieldLabelKey !== 0) {
                        $label .= ' — ';
                    }

                    $label .= $item->{$fieldLabel};
                }

            }
        } else {
            $label = $item->{$field};
        }

        return $label;
    }
}
