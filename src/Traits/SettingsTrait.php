<?php

namespace VY\WAdmin\Traits;

use VY\WAdmin\Models\Media;

trait SettingsTrait
{
    static function varexport($expression, $indent = 4)
    {
        $object = json_decode(str_replace(['(', ')'], ['&#40', '&#41'], json_encode($expression)), true);
        $export = str_replace(['array (', ')', '&#40', '&#41'], ['[', ']', '(', ')'], var_export($object, true));
        $export = preg_replace("/ => \n[^\S\n]*\[/m", ' => [', $export);
        $export = preg_replace("/ => \[\n[^\S\n]*\]/m", ' => []', $export);
        $spaces = str_repeat(' ', $indent);
        $export = preg_replace("/([ ]{2})(?![^ ])/m", $spaces, $export);
        $export = preg_replace("/^([ ]{2})/m", $spaces, $export);

        return $export;
    }

    public function fieldsProcessing($fields, $model = false)
    {
        if ($model) {
            $model = $model->toArray();
        }

        foreach ($fields as $tabName => &$tab) {
            foreach ($tab as $fieldName => &$field) {
                $field['tab'] = $tabName;

                if ($field['type'] === 'password') {
                    $field['val'] = '';
                } else if ($field['type'] === 'multiple-images' || $field['type'] === 'multiple-files') {
                    foreach ($field['val'] as $key => $id) {
                        $field['val'][$key] = Media::find($id);
                    }
                } else if (($field['type'] === 'image' || $field['type'] === 'file') && !empty($field['val'])) {
                    $field['val'] = Media::find($field['val']);
                } else if (!empty($field['relationship'])) {
                    $field = $this->assignRelation($field, $model);
                } else {
                    if ($model) {
                        $field['val'] = $model[$field['field']];
                    }
                    if (!isset($field['val'])) {
                        $field['val'] = '';
                    }
                }
            }
        }

        return $fields;
    }
}
