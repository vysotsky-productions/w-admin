<?php

namespace VY\WAdmin\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\Console\Input\InputOption;

class UserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'w-admin:user {email?} {--create}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create user and add it to database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $create = $this->option('create');
        $email = $this->argument('email');

        if ($create) {
            $name = $this->ask('Enter the admin name');

            while (!$email) {
                $email = $this->ask('Enter the user email');
            }

            $password = $this->secret('Enter user password');

            $model = config('w-admin.user.namespace', 'App\\User');

            $model::create([
                'name'     => $name,
                'email'    => $email,
                'password' => Hash::make($password),
            ]);
        }
    }
}