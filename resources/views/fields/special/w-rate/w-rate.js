import fieldMixin from './../../../../assets/js/mixins/field';

export default {
    mixins: [fieldMixin],
}