import fieldMixin from './../../../../assets/js/mixins/field';

export default {
    mixins: [fieldMixin],
    data() {
        return {
            passwordType: 'password'
        }
    },
    methods: {
        togglePasswordType: function () {
            if (this.passwordType === 'password') {
                this.passwordType = 'text';
            } else {
                this.passwordType = 'password';
            }
        }
    }
}