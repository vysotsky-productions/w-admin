import fieldMixin from './../../../../assets/js/mixins/field';

export default {
    mixins: [fieldMixin],
    computed: {
        min() {
            if (this.options && this.options.min) {
                return this.options.min;
            } else {
                return 0;
            }
        },
        max() {
            if (this.options && this.options.max) {
                return this.options.max;
            } else {
                return 100;
            }
        },
        step() {
            if (this.options && this.options.step) {
                return this.options.step;
            } else {
                return 1;
            }
        },
    }
}