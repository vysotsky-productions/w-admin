import fieldMixin from './../../../../assets/js/mixins/field';

export default {
    mixins: [fieldMixin],
    data() {
        return {
            headers: {
                'X-CSRF-TOKEN': window.Laravel.csrfToken,
                'X-Requested-With': 'XMLHttpRequest'
            },
            image: null
        }
    },
    methods: {
        handleSuccess(response) {
            if (response.media !== undefined) {
                this.field.val.push(response.media);
            } else {
                this.uploadingError();
            }
        },
        uploadingError() {
            EventBus.$emit('show_notification', {
                status: 'error',
                notification: 'Error Uploading'
            });
        },
        remove(key) {
            this.value.splice(key, 1);
        },
    }
}