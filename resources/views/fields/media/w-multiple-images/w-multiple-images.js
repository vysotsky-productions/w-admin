import fieldMixin from './../../../../assets/js/mixins/field';

export default {
    mixins: [fieldMixin],
    data() {
        return {
            headers: {
                'X-CSRF-TOKEN': window.Laravel.csrfToken,
                'X-Requested-With': 'XMLHttpRequest'
            },
            image: null
        }
    },
    mounted() {
        EventBus.$off('cropped_' + this.name);
        EventBus.$on('cropped_' + this.name, (media) => {
            this.field.val.push(media);
        });
    },
    methods: {
        load() {
            let file = this.$refs[this.name].files[0];

            if (file) {
                let reader = new FileReader();

                reader.onload = (event) => {
                    EventBus.$emit('cropper', {
                        name: this.name,
                        file: event.target.result,
                        ratio: this.cropperRatio,
                        width: this.cropperWidth,
                        height: this.cropperHeight
                    });
                };
                reader.readAsDataURL(file);

                this.$refs[this.name].value = '';
            }
        },
        remove(key) {
            this.value.splice(key, 1);
        },
    },
    computed: {
        cropperWidth() {
            if (this.field.cropper && this.field.cropper.width) {
                return this.field.cropper.width;
            } else {
                return null;
            }
        },
        cropperHeight() {
            if (this.field.cropper && this.field.cropper.height) {
                return this.field.cropper.height;
            } else {
                return null;
            }
        },
        cropperRatio() {
            if (this.field.cropper && this.field.cropper.ratio) {
                return this.field.cropper.ratio;
            } else {
                return null;
            }
        }
    },
}