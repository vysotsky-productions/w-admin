import fieldMixin from './../../../../assets/js/mixins/field';

export default {
    mixins: [fieldMixin],
    data() {
        return {
            headers: {
                'X-CSRF-TOKEN': window.Laravel.csrfToken,
                'X-Requested-With': 'XMLHttpRequest'
            },
        }
    },
    computed: {
        value() {
            if (this.field.val && this.field.val.path) {
                return this.field.val.path;
            } else {
                return null;
            }
        }
    },
    methods: {
        remove() {
            this.field.val = null;
        },
        handleSuccess(response) {
            this.field.val = response.media;
        }
    }
}