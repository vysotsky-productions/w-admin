import fieldMixin from './../../../../assets/js/mixins/field';

export default {
    mixins: [fieldMixin],
    computed: {
        start() {
            if (this.options && this.options.start !== undefined) {
                return this.options.start;
            } else {
                return '09:00';
            }
        },
        end() {
            if (this.options && this.options.end !== undefined) {
                return this.options.end;
            } else {
                return '18:00';
            }
        },
        step() {
            if (this.options && this.options.step) {
                return this.options.step;
            } else {
                return '00:30';
            }
        },
        pickerOptions(){
            return {
                start: this.start,
                end: this.end,
                step: this.step,
            };
        }
    }
}