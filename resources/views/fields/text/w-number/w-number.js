import fieldMixin from './../../../../assets/js/mixins/field';

export default {
    mixins: [fieldMixin],
    computed: {
        value: {
            get() {
                return this.field.val;
            },
            set(value) {
                if (value !== undefined || value === '') {
                    this.field.val = value;
                } else {
                    this.field.val = '';
                }
            }
        },
        label() {
            if (this.min !== -Infinity && this.max !== Infinity) {
                return `${this.name} (range: ${this.min} - ${this.max})`;
            } else if (this.min !== -Infinity) {
                return `${this.name} (min: ${this.min})`;
            } else if (this.max !== Infinity) {
                return `${this.name} (max: ${this.max})`;
            } else {
                return this.name;
            }
        },
        min() {
            if (this.options && this.options.min) {
                return this.options.min;
            } else {
                return -Infinity;
            }
        },
        max() {
            if (this.options && this.options.max) {
                return this.options.max;
            } else {
                return Infinity;
            }
        }
    }
}