import MediumEditor from 'medium-editor';
import fieldMixin from './../../../../assets/js/mixins/field';
// import * as MediumInsert from 'medium-editor-insert-plugin/dist/js/medium-editor-insert-plugin';
import * as MediumInsert from './../../../../assets/js/libs/medium-editor-insert-plugin/dist/js/medium-editor-insert-plugin.min';

window["MediumInsert"] = MediumInsert.MediumInsert;

export default {
    mixins: [fieldMixin],
    data() {
        return {
            editor: null,
            content: null,
            headers: {
                'X-CSRF-TOKEN': window.Laravel.csrfToken,
                'X-Requested-With': 'XMLHttpRequest'
            }
        }
    },
    mounted() {
        this.createEditor();

        EventBus.$on('before_save', () => this.change());
        EventBus.$on('model_changed', () => setTimeout(() => {
            this.createEditor();
        }, 1));
    },

    methods: {
        createEditor() {
            let id = '#' + this.id;

            this.content = this.field.val;

            if(this.editor){
                this.editor.destroy();
            }

            this.editor = new MediumEditor(id, {
                disableEditing: this.disabled,
                toolbar: {
                    allowMultiParagraphSelection: true,
                    buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
                    diffLeft: 0,
                    diffTop: -10,
                    firstButtonClass: 'medium-editor-button-first',
                    lastButtonClass: 'medium-editor-button-last',
                    relativeContainer: null,
                    standardizeSelectionStart: false,
                    static: false,
                    align: 'center',
                    sticky: false,
                    updateOnEmptySelection: false,
                },
                placeholder: {
                    text: '',
                    hideOnClick: true
                }
            });

            if (!this.disabled) {
                $(id).mediumInsert({
                    editor: this.editor,
                    addons: {
                        images: {
                            autoGrid: 2,
                            deleteScript: false,
                            fileUploadOptions: {
                                url: '/w-admin/api/media/save_for_editor',
                                headers: this.headers,
                            },
                            uploadCompleted: ($el, data) => this.change(),
                        },
                    },
                });
            }
        },

        change() {
            let content = this.editor.serialize();

            if (content[this.id] !== undefined && content[this.id].value !== undefined) {
                this.value = content[this.id]['value'];
            }
        }
    },
}