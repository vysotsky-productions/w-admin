import fieldMixin from './../../../../assets/js/mixins/field';

export default {
    mixins: [fieldMixin],
    inject: ['$validator'],
    computed: {
        link() {
            if (this.value) {
                return this.linkPrefix + this.value + this.linkPostfix;
            } else if (this.showEmpty) {
                return this.linkPrefix + this.linkPostfix;
            }
        },
        linkPrefix() {
            if (this.field.prefix !== undefined && this.field.prefix) {
                return this.field.prefix;
            } else {
                return '';
            }
        },
        linkPostfix() {
            if (this.field.postfix !== undefined && this.field.postfix) {
                return this.field.postfix;
            } else {
                return '';
            }
        },
        showEmpty() {
            if (this.field.show_empty !== undefined) {
                return this.field.show_empty;
            }

            return false;
        },
        isVisible() {
            if (this.value) {
                return true;
            } else{
                return this.showEmpty;
            }
        }
    }
}