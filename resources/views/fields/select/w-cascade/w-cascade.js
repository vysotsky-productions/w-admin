import fieldMixin from './../../../../assets/js/mixins/field';

export default {
    mixins: [fieldMixin],
    methods: {
        change(value){
            this.value = value;
        }
    }
}