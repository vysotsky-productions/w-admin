import fieldMixin from './../../../../assets/js/mixins/field';

export default {
    mixins: [fieldMixin],
    computed: {
        value: {
            get() {
                if (this.field.val !== undefined) {
                    if (this.field.val === 1 || this.field.val === true) {
                        return true;
                    } else {
                        return false
                    }
                } else {
                    return false;
                }
            },
            set(value) {
                this.field.val = value;
            }
        },
        activeLabel() {
            if (this.n && this.values[0]) {
                return this.values[0];
            } else {
                return 'Disabled';
            }
        },
        disabledLabel() {
            if (this.values && this.values[1]) {
                return this.values[1];
            } else {
                return 'Active';
            }
        }
    }
}