import fieldMixin from './../../../../assets/js/mixins/field';

export default {
    mixins: [fieldMixin],
    value: {
        get() {
            if (this.field.val !== undefined) {
                return this.field.val;
            } else {
                return '';
            }
        },
        set(value) {
            this.field.val = value;
        }
    },
}