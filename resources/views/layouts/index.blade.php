<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="/vendor/vy/w-admin/public/app.css">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script> window.Laravel = {!! json_encode([ 'csrfToken' => csrf_token()]) !!};</script>
</head>
<body id="w-admin_body">

<div id="app">
    <app-header></app-header>
    <app-menu v-if="$store.getters.authenticated"></app-menu>

    <app-notification v-if="$store.getters.authenticated"></app-notification>
    <app-popup v-if="$store.getters.authenticated"></app-popup>
    <cropper v-if="$store.getters.authenticated"></cropper>

    <div class="content">
        <transition>
            <router-view></router-view>
        </transition>
    </div>
</div>

<div class="desktop-only">Desktop only</div>

<script src="/vendor/vy/w-admin/public/app.js"></script>
</body>

</html>