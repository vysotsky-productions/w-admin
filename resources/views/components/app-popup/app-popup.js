export default {
    data() {
        return {
            title: null,
            status: null,
            data: null,
            actionEvent: null,
            buttonClass: 'default',
            buttonText: null,
            visible: false
        }
    },
    created() {
        this.popupListener();
    },
    methods: {
        popupListener() {
            EventBus.$on('show_popup', (data) => this.showPopup(data));
        },
        showPopup(data) {
            this.visible = true;
            this.title = data.title;
            this.buttonText = data.buttonText;
            this.actionEvent = data.actionEvent;
            this.data = data.data;
            this.buttonClass = data.buttonClass;
        },
        hidePopup() {
            this.visible = false;

            setTimeout(() => {
                this.title = null;
                this.status = null;
                this.data = null;
                this.actionEvent = null;
                this.buttonText = null;
                this.buttonClass = 'default';
            }, 400);
        },
        makeAction() {
            EventBus.$emit(this.actionEvent, this.data);
            this.hidePopup();
        }
    }
}