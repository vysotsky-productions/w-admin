export default {
    data() {
        return {
            vendorPath: window.vendorPath,
            searchPhrase: null
        }
    },
    methods: {
        saveModel() {
            EventBus.$emit('model_save');
        },
        saveSettings() {
            EventBus.$emit('settings_save');
        },
        addNewItem() {
            EventBus.$emit('model_add-new-item');
        },
        search() {
            EventBus.$emit('model_search', this.searchPhrase);
        }
    }
};