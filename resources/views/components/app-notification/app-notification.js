export default {
    data() {
        return {
            notification: null,
            status: null,
            timeout: null
        }
    },
    created() {
        this.notificationListener();
    },
    methods: {
        notificationListener() {
            EventBus.$on('show_notification', (data) => this.showNotification(data));
        },
        showNotification(data) {
            this.status = data.status;
            this.notification = data.notification;
            this.hideNotification();
        },
        hideNotification() {
            clearTimeout(this.timeout);
            this.timeout = setTimeout(() => this.clearNotification(), 800);
        },
        clearNotification() {
            this.status = null;
        }
    },
    computed: {
        statusClass() {
            if (this.status) {
                return 'mod_' + this.status;
            }
        }
    }
}