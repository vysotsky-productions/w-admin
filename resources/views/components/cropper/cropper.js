import Cropper from "cropperjs";

export default {
    data() {
        return {
            vendorPath: window.vendorPath,
            cropperObject: false,
            visible: false,
            cropper: {
                file: false,
                ratio: false,
                width: 1920,
                height: 1080,
                name: false
            }
        };
    },
    mounted() {
        EventBus.$on('cropper', (data) => {
            this.cropper.name = data.name;
            this.cropper.file = data.file;
            this.cropper.ratio = data.ratio;
            this.cropper.width = data.width;
            this.cropper.height = data.height;
            this.visible = true;

            setTimeout(() => {
                this.cropperObject = new Cropper(document.getElementById('cropper'), {
                    aspectRatio: this.cropper.ratio,
                    autoCropArea: 1
                });
            }, 20);
        });
    },
    methods: {
        save() {
            this.cropperObject.getCroppedCanvas({
                width: this.cropper.width,
                height: this.cropper.height,
                imageSmoothingQuality: "high"
            }).toBlob((blob) => {
                let data = new FormData();
                data.append('photo', blob);

                this.axios.post('/w-admin/api/media/save', data).then((responce) => {
                    if (responce.data.status === 'success') {
                        EventBus.$emit('cropped_' + this.cropper.name, responce.data.media);
                        this.close();
                    } else {
                        EventBus.$emit('show_notification', {status: 'error', notification: 'Server Error'});
                        this.close();
                    }
                }).catch(() => {
                    EventBus.$emit('show_notification', {status: 'error', notification: 'Server Error'});
                });
            });
        },
        close() {
            this.visible = false;
            this.cropperObject.destroy();
        }
    }
}