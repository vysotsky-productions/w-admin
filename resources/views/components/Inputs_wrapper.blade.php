<div class="inputs-wrapper_input">
    @if(!empty($key))
        <span class="input_title">{{$key}}</span>
    @endif

    @if($type == 'text')
        <wadmin_text name="{{$input_name}}" class="input_content"
                     value="{{$val}}"></wadmin_text>
    @elseif($type == 'textarea')
        <wadmin_textarea name="{{$input_name}}" class="input_content"
                         value="{{$val}}"></wadmin_textarea>
    @elseif($type == 'radio')
        @if(is_array($val))
            @foreach($val as $value)
                <wadmin_radio name="{{$input_name}}" class="input_content"
                              value="{{$value}}"></wadmin_radio>
            @endforeach
        @else
            <wadmin_radio name="{{$input_name}}" class="input_content"
                          value="{{$val}}"></wadmin_radio>
        @endif

    @elseif($type == 'select')
        <wadmin_select
                name="{{$input_name}}"
                class="input_content"
                :options="{{json_encode($input['options'])}}"
                value="{{$val}}"
        ></wadmin_select>

    @elseif($type == 'image')
        <wadmin_image name="{{$input_name}}" class="input_content"
                      value="{{$val}}"></wadmin_image>
    @elseif($type == 'checkbox')
        <wadmin_checkbox name="{{$input_name}}" class="input_content"
                         value="{{$val}}"></wadmin_checkbox>
    @elseif($type == 'password')
        <wadmin_password name="{{$input_name}}" value="{{$val}}" class="input_content" ></wadmin_password>
    @endif
    @if(isset($alias) && !empty($alias))
        <input type="hidden" name="{{$alias.';'.$key.';type'}}" value="{{$type}}">
    @endif
</div>