export default {
    data() {
        return {
            settings: {},
            activeTab: 'tab-0'
        }
    },
    created() {
        this.getSettings();
        this.breadcrumbs();
        this.saveListener();
    },
    methods: {
        getSettings() {
            this.axios.post('/w-admin/api/settings').then((response) => {
                this.settings = response.data;
            });
        },
        breadcrumbs() {
            this.$store.commit('breadcrumbs', [
                {
                    path: null,
                    title: 'Settings'
                }
            ]);
        },
        saveListener() {
            EventBus.$on('settings_save', () => this.save());
        },
        save() {
            this.$validator.validateAll().then((result) => {
                if (result) {
                    EventBus.$emit('before_save');
                    this.axios.post('/w-admin/api/settings/save', this.settings).then((response) => {
                        EventBus.$emit('show_notification', {
                            status: response.data.status,
                            notification: response.data.notification
                        });

                        EventBus.$emit('after_save');
                    }).catch(function (error) {
                        EventBus.$emit('show_notification', {
                            status: 'error',
                            notification: 'Server Error'
                        });
                    });
                } else {
                    EventBus.$emit('show_notification', {
                        status: 'error',
                        notification: 'Validation Error'
                    });
                }
            }).catch(() => {
                EventBus.$emit('show_notification', {
                    status: 'error',
                    notification: 'Validation Error'
                });
            });
        },
        tabNameWithSpaces(text) {
            return text.replace(/-/g, ' ');
        },
    },
    beforeDestroy() {
        EventBus.$off('settings_save');
    }
};