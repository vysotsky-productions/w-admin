// import VeeValidate from 'vee-validate';
// import Vue from 'vue';
// Vue.use(VeeValidate);

export default {
    data() {
        return {
            list: null,
            model: null,
            activeTab: 'tab-0',

            // Pagination
            currentPage: 1,
            itemsPerPage: 30,
            countAllItems: null,
            searchPhrase: null,

            // Order
            orderField: null,
            orderDirection: null
        }
    },

    created() {
        this.getModel();
        this.listeners();

        this.$store.state.validator = this.$validator;
    },
    watch: {
        '$route'(to, from) {
            if (to.params.slug === from.params.slug) {
                this.getModel(this.currentPage);
            } else {
                this.orderField = null;
                this.orderDirection = null;
                this.currentPage = 1;

                this.getModel();
            }

            this.errors.clear();
            this.activeTab = 'tab-0';
        }
    },
    methods: {
        getModel() {
            this.axios.post(this.link, {
                page: this.currentPage,
                searchPhrase: this.searchPhrase,
                orderField: this.orderField,
                orderDirection: this.orderDirection
            }).then((response) => {
                if (!response.data.item_exists && this.$route.params.id) {
                    return this.$router.push('/w-admin/model/' + this.slug);
                }

                this.model = response.data.model;
                this.countAllItems = response.data.countAllItems;


                if (response.data.list) {
                    this.list = response.data.list;
                }

                this.updateSearchFields();
                this.breadcrumbs();

                EventBus.$emit('model_changed');
            }).catch((error) => {
                this.model = null;
                this.list = null;

                this.updateSearchFields();
                this.$store.commit('breadcrumbs', null);

                EventBus.$emit('show_notification', {
                    status: 'error',
                    notification: 'Server Error'
                });
            });
        },
        breadcrumbs() {
            this.$store.commit('breadcrumbs', [
                {
                    title: 'Models',
                    path: null
                }, {
                    title: this.name,
                    path: null
                }, {
                    title: this.id ? 'Edit' : 'Add new',
                    path: null
                }
            ]);
        },
        listeners() {
            EventBus.$on('model_save', () => this.save());
            EventBus.$on('model_add-new-item', () => this.routeToCreateNewItem());
            EventBus.$on('model_remove-item', (data) => this.remove(data));
            EventBus.$on('model_search', (phrase) => this.search(phrase));
            EventBus.$on('model_remove-popup-current', () => this.removeCurrent());
        },
        save() {
            this.$validator.validateAll().then((result) => {
                if (result) {
                    EventBus.$emit('before_save');
                    this.axios.post('/w-admin/api/model/' + this.slug + '/' + this.id + '/save', {
                        fields: this.model.fields,
                        page: this.currentPage,
                        searchPhrase: this.searchPhrase,
                        orderField: this.orderField,
                        orderDirection: this.orderDirection
                    }).then((response) => {
                        this.model = response.data.model;
                        this.countAllItems = response.data.countAllItems;
                        this.list = response.data.list;

                        if (response.data.id && !this.id) {
                            this.routeToItem(response.data.id);
                        }

                        EventBus.$emit('show_notification', {
                            status: response.data.status,
                            notification: response.data.notification
                        });

                        EventBus.$emit('after_save');
                    }).catch((error) => {
                        EventBus.$emit('show_notification', {
                            status: 'error',
                            notification: 'Server Error'
                        });
                    });
                } else {
                    EventBus.$emit('show_notification', {
                        status: 'error',
                        notification: 'Validation Error'
                    });
                }
            }).catch(() => {
                EventBus.$emit('show_notification', {
                    status: 'error',
                    notification: 'Validation Error'
                });
            });
        },
        tabNameWithSpaces(text) {
            return text.replace(/-/g, ' ');
        },
        routeToItem(id) {
            this.$router.push('/w-admin/model/' + this.slug + '/' + id);
        },
        routeToCreateNewItem() {
            this.$router.push('/w-admin/model/' + this.slug);
        },
        formatField(field) {
            if (field) {
                return field.replace(/-/g, ' ').replace(/_/g, ' ');
            } else {
                return '';
            }
        },
        removePopup(id) {
            EventBus.$emit('show_popup', {
                data: {id: id},
                title: 'Do you really want to delete the item?',
                buttonText: 'Remove',
                buttonClass: 'remove',
                actionEvent: 'model_remove-item',
            });
        },
        removeCurrent() {
            if (this.id) {
                this.removePopup(this.id)
            }
        },
        remove(data) {
            this.axios.post('/w-admin/api/model/' + this.slug + '/' + data.id + '/remove', {
                page: this.currentPage,
                searchPhrase: this.searchPhrase,
                orderField: this.orderField,
                orderDirection: this.orderDirection
            }).then((response) => {
                if (response.data.list) {
                    this.list = response.data.list;
                    this.countAllItems = response.data.countAllItems;
                }
                if (data.id == this.id) {
                    this.$router.push('/w-admin/model/' + this.slug);
                }
            }).catch((error) => {
                EventBus.$emit('show_notification', {
                    status: 'error',
                    notification: 'Server Error'
                });
            });
        },
        search(searchPhrase) {
            this.searchPhrase = searchPhrase;
            this.currentPage = 1;
            this.getModel();
        },
        updateSearchFields() {
            if (this.model && this.model.search) {
                this.$store.commit('setSearchFields', this.model.search);
            } else {
                this.$store.commit('setSearchFields', null);
            }
        },
        paginate(page) {
            if (typeof page === "number") {
                this.currentPage = page;
                this.getModel();
            }
        },
        order(field) {
            if (this.orderField === field.field && this.orderDirection === 'asc') {
                this.orderDirection = 'desc';
            } else if (this.orderField === field.field && this.orderDirection === 'desc') {
                this.orderDirection = null;
                this.orderField = null;
            } else {
                this.orderField = field.field;
                this.orderDirection = 'asc';
            }

            this.currentPage = 1;
            this.getModel();
        },
        orderClass(field) {
            let orderClass = '';
            if (field.field === this.orderField) {
                orderClass = 'mod_' + this.orderDirection;
            }
            return orderClass;
        }
    },
    computed: {
        slug() {
            if (this.$route.params.slug) {
                return this.$route.params.slug;
            } else {
                return '';
            }
        },
        id() {
            if (this.$route.params.id !== undefined && this.$route.params.id) {
                return this.$route.params.id;
            } else {
                return 0;
            }
        },
        name() {
            if (this.model.name) {
                return this.model.name;
            } else {
                return '';
            }
        },
        link() {
            if (this.slug && this.id) {
                return '/w-admin/api/model/' + this.slug + '/' + this.id;
            } else if (this.slug) {
                return '/w-admin/api/model/' + this.slug;
            } else {
                return '';
            }
        },
        pagination() {
            let links = [];
            let countPages = Math.ceil(this.countAllItems / this.itemsPerPage);

            if (countPages === 1) {
                return null;
            } else if (countPages < 8) {
                for (let page = 1; page <= countPages; page++) {
                    links.push({page: page, type: page === this.currentPage ? 'active' : 'default'});
                }
                return links;
            } else if (countPages >= 8 && this.currentPage <= 3) {
                for (let page = 1; page <= 4; page++) {
                    links.push({page: page, type: page === this.currentPage ? 'active' : 'default'});
                }
                links.push({page: '...', type: 'disable'});
                links.push({page: countPages, type: 'default'});

                return links;
            } else if (countPages >= 8 && this.currentPage >= countPages - 2) {
                links.push({page: 1, type: 'default'});
                links.push({page: '...', type: 'disable'});

                for (let page = countPages - 3; page <= countPages; page++) {
                    links.push({page: page, type: page === this.currentPage ? 'active' : 'default'});
                }
                return links;
            } else {
                links.push({page: 1, type: 'default'});
                links.push({page: '...', type: 'disable'});
                links.push({page: this.currentPage - 1, type: 'default'});
                links.push({page: this.currentPage, type: 'active'});
                links.push({page: this.currentPage + 1, type: 'default'});
                links.push({page: '...', type: 'disable'});
                links.push({page: countPages, type: 'default'});

                return links;
            }
        },
    },
    beforeDestroy() {
        EventBus.$off('model_save');
        EventBus.$off('model_add-new-item');
        EventBus.$off('model_remove-item');
        EventBus.$off('model_search');
        EventBus.$off('model_remove-popup-current');
    }
};