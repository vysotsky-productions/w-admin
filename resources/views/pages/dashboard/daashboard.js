export default {
    created() {
        this.breadcrumbs();
    },
    methods: {
        breadcrumbs() {
            this.$store.commit('breadcrumbs', [
                {
                    path: null,
                    title: 'Dashboard'
                }
            ]);
        },
    },
}