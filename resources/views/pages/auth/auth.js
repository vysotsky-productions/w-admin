import {apiGetUser} from './../../../assets/js/api';

export default {
    data() {
        return {
            email: '',
            password: '',
            errorMessage: ''
        }
    },
    methods: {
        logIn: function () {
            this.$validator.validateAll().then((result) => {
                if (result) {
                    this.axios.post('/w-admin/login', {
                        email: this.email,
                        password: this.password
                    }).then((response) => {
                        if (response.data.status === 'authenticated') {
                            apiGetUser(() => {
                                this.$router.push('/w-admin/dashboard');
                            });
                        }
                    }, (error) => {
                        this.errorMessage = 'Wrong password or email!';
                    });
                } else {
                    this.errorMessage = 'Fill all fields!';
                }
            });
        }, cleanErrors: function () {
            this.errorMessage = '';
        }
    }
};