import axios from 'axios';
import awesome from './libs/fontawesome';


// Const
window.vendorPath = '/vendor/vy/w-admin/public/';
global.jQuery = require('jquery');
global.$ = require('jquery');

// Set Axios For Laravel
axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

// Init Vue
require('./vue/app');

// Init Lodash
window._ = require('lodash');

