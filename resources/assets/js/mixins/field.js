export default {
    inject: ['$validator'],
    props: ['field'],
    computed: {
        value: {
            get() {
                if (this.field.val !== undefined) {
                    return this.field.val;
                } else {
                    return '';
                }
            },
            set(value) {
                this.field.val = value;
            }
        },
        name() {
            return this.field.field;
        },
        tab() {
            return this.field.tab;
        },
        id() {
            return this.tab + '_' + this.name;
        },
        label() {
            if (this.field.label) {
                return this.field.label;
            } else if (this.name) {
                return this.name.replace(/-/g, ' ').replace(/_/g, ' ');
            } else {
                return '';
            }
        },
        disabled() {
            if (this.field.disabled) {
                return this.field.disabled;
            } else {
                return false;
            }
        },
        options() {
            if (this.field.options) {
                return this.field.options;
            } else {
                return {};
            }
        },
        values() {
            if (this.field.values) {
                return this.field.values;
            } else {
                return {};
            }
        },
        rules() {
            if (this.field.rules !== undefined && this.field.rules) {
                return this.field.rules;
            } else {
                return '';
            }
        }
    }
};