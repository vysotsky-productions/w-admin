import axios from "axios/index";
import {store} from "./vue/app";

let adminPath = 'w-admin';

export const apiGetUser = function (callback) {
    axios.post(`/${adminPath}/api/get_user`).then((response) => {
        if (response.data.user && response.data.settings) {
            store.commit('setUser', response.data.user);
            store.commit('setSettings', response.data.settings);

            if (typeof callback === 'function') {
                callback();
            }
        } else {
            store.commit('setUser', null);
        }
    }).catch((error) => {
        store.commit('setUser', null);
    });
};
