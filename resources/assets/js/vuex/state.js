export default {
    user: null,
    settings: null,
    breadcrumbs: null,
    searchPhrase: null,
    searchFields: null,
    eventCharData: null,
}