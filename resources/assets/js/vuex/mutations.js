export default {
    setUser(state, user) {
        state.user = user;
    },
    setSettings(state, settings) {
        state.settings = settings;
    },
    breadcrumbs(state, breadcrumbs) {
        state.breadcrumbs = breadcrumbs;
    },
    setSearchFields(state, searchFields) {
        state.searchFields = searchFields;
    },
    setCharEvents(state,value){
        state.eventCharData = value;
    }
};