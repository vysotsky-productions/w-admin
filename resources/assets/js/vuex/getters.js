export default {
    authenticated: function (state) {
        return !!state.user;
    },
    getEventCharData: function (state) {
        return state.eventCharData;
    },
    getSearchFields(state) {
        return state.searchFields;
    },
    appName(state) {
        if (state.settings && state.settings !== undefined) {
            return state.settings.appName;
        } else {
            return 'App Name';
        }
    },
    models(state) {
        if (state.settings && state.settings.models !== undefined) {
            return state.settings.models;
        } else {
            return null;
        }
    }
};