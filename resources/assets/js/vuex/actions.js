import axios from 'axios'

export default {
    getChartData({commit}, {table, month, year}) {
        axios.get('/w-admin/api/chart-data', {
            params: {
                table: table,
                month: month,
                year: year
            }
        }).then((response)=>{
            commit('setCharEvents',response.data)
        })
    }

};