import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import VeeValidate from 'vee-validate';
import {sync} from 'vuex-router-sync';
import routes from './routes';
import axios from 'axios';
import {apiGetUser} from './../api';
import Chartkick from 'chartkick'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'

Vue.use(VueChartkick, {Chartkick});

import {
    Input,
    Tabs,
    TabPane,
    Switch,
    InputNumber,
    Select,
    Option,
    Checkbox,
    CheckboxGroup,
    Radio,
    RadioGroup,
    DatePicker,
    TimePicker,
    TimeSelect,
    Upload,
    Dialog,
    Slider,
    Rate,
    Cascader,
    Scrollbar
} from 'element-ui';
import elementLang from 'element-ui/lib/locale/lang/en';
import elementLocale from 'element-ui/lib/locale';

// Vuex Parts
import state from './../vuex/state';
import getters from './../vuex/getters';
import mutations from './../vuex/mutations';
import actions from './../vuex/actions';

require('./autoload.js');

// Element Components
elementLocale.use(elementLang);
Vue.component(Scrollbar.name, Scrollbar);
Vue.component(Input.name, Input);
Vue.component(Tabs.name, Tabs);
Vue.component(TabPane.name, TabPane);
Vue.component(Switch.name, Switch);
Vue.component(InputNumber.name, InputNumber);
Vue.component(Select.name, Select);
Vue.component(Cascader.name, Cascader);
Vue.component(Option.name, Option);
Vue.component(Checkbox.name, Checkbox);
Vue.component(CheckboxGroup.name, CheckboxGroup);
Vue.component(Radio.name, Radio);
Vue.component(RadioGroup.name, RadioGroup);
Vue.component(DatePicker.name, DatePicker);
Vue.component(TimePicker.name, TimePicker);
Vue.component(TimeSelect.name, TimeSelect);
Vue.component(Upload.name, Upload);
Vue.component(Dialog.name, Dialog);
Vue.component(Slider.name, Slider);
Vue.component(Rate.name, Rate);


Vue.component('w-scrollbar', {
    extends: Scrollbar,
    inject: ['$validator']
});

Vue.component('w-tabs', {
    extends: Tabs,
    inject: ['$validator']
});

Vue.component('w-tab-pane', {
    extends: TabPane,
    inject: ['$validator']
});


// Vue Use
Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);

// Init Event Bus
window.EventBus = new Vue();

// Init Store
export const store = new Vuex.Store({
    axios, state, getters, mutations, actions
});

// Init Router
const router = new VueRouter(routes);
sync(store, router);


// Vee Validate
const veeValidateConfig = {
    classes: false,
    events: 'blur',
};

Vue.use(VeeValidate, veeValidateConfig);


// Init Vue
new Vue({
    router,
    store,
    axios,
    el: '#app',
    mounted() {
        apiGetUser(() => {
            if (this.$route.name === 'login' && this.$store.getters.authenticated) {
                this.$router.push('dashboard');
            }
        });
    }
});

$(window).bind('keydown', function (event) {
    if (event.ctrlKey || event.metaKey) {
        if (String.fromCharCode(event.which).toLowerCase() === 's') {
            event.preventDefault();
            EventBus.$emit('settings_save');
            EventBus.$emit('model_save');
        }
    }
});

$(window).bind('keydown', function (event) {
    if (event.shiftKey && event.keyCode === 46) {
        event.preventDefault();
        EventBus.$emit('model_remove-popup-current');
    }
});