export default {
    mode: 'history',
    routes: [
        {
            path: '/w-admin/login',
            name: 'login',
            component: require('./../../../views/pages/auth/auth.vue')
        }, {
            path: '/w-admin/dashboard',
            name: 'dashboard',
            component: require('./../../../views/pages/dashboard/daashboard.vue')
        }, {
            path: '/w-admin/settings',
            name: 'settings',
            component: require('./../../../views/pages/settings/settings.vue')
        }, {
            path: '/w-admin/model/:slug',
            name: 'model-create',
            component: require('./../../../views/pages/model/model.vue')
        }, {
            path: '/w-admin/model/:slug/:id',
            name: 'model-edit',
            component: require('./../../../views/pages/model/model.vue')
        },
    ]
};