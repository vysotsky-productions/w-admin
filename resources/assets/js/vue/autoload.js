import Vue from 'vue';

var require_vue = require.context('./../../../', true, /\.(vue)$/i);

require_vue.keys().map(key => {
    const name = key.split('/').reverse()[1];
    return Vue.component(name, require_vue(key));
});