# W-Admin
version 0.2.0 alpha

## Install

1. Require composer package
```
"vy/w-admin": "*"
```

* for dev
```
"autoload": {
    "psr-4": {
        "App\\": "app/",
        "VY\\WAdmin\\": "./../w-admin/src"
    }
},
"repositories": [
    {
        "type": "path",
        "url": "./../w- admin/"
    }
],
```


1. Add to 'config/app.php'
```
VY\WAdmin\WAdminServiceProvider::class
```

2. Publish config
```
php artisan vendor:publish
```

3. Add middleware group at 'app/Http/Kernel.php'
```
'vy-admin' => []
```

***
Rewrite Auth namespace, add to config/app.php
```
'vyAdmin' => [
    'authNamespace' => 'App\Http\Controllers\VYAdmin\\'
],
```


***
To create symbolic link for develop. If you're a Windows user you need to put it to "cmd console" as ADMIN  
```
mklink /D "C:\OSPanel\domains\lamoore\public\vendor\vy\w-admin\public" "C:\OSPanel\domains\w-admin\publishable\public"
```


