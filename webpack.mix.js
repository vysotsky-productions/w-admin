let {mix} = require('laravel-mix');
const path = require('path');
const globImporter = require('node-sass-glob-importer');


mix.autoload({
    jquery: ['$', 'window.jQuery', 'window.$'],
}).webpackConfig({
    resolve: {
        alias: {
            'jquery-ui/widget': 'blueimp-file-upload/js/vendor/jquery.ui.widget.js'
        }
    }
}).sass('./resources/assets/scss/app.scss', 'publishable/public/app.css', {importer: globImporter()})
    .js('./resources/assets/js/app.js', __dirname + 'publishable/public/app.js')
    .sourceMaps();